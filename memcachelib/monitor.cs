﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.Threading;

namespace thesebas.memcache.monitor
{

    public class MemcacheMonitorException : Exception
    {
        public MemcacheMonitorException(string message) : base(message) { }
    }
    public class MemcacheMonitorManager
    {
        Thread monthread;
        public List<MemcacheMonitor> monitors;
        public delegate void DataReadyDelegate(MemcacheMonitorManager manager);
        public event DataReadyDelegate DataReady;
        public MemcacheMonitorManager()
        {
            this.monthread = new Thread(new ThreadStart(this.monitor));
            this.monitors = new List<MemcacheMonitor>();
            this.DataReady += new DataReadyDelegate(delegate(MemcacheMonitorManager manager) {/* dummy delegate */});
        }
        public void StartMonitoring()
        {
            this.monthread.Start();

        }

        public MemcacheMonitor AddMonitor(MemcacheMonitor monitor)
        {

            this.monitors.Add(monitor);
            return monitor;
        }
        public void monitor()
        {
            //initial read
            foreach (MemcacheMonitor m in this.monitors)
            {
                m.GetStatsData();
            }

            Thread.Sleep(1000);

            //main loop
            while (true)
            {
                foreach (MemcacheMonitor m in this.monitors)
                {
                    m.GetStatsData();
                }
                this.DataReady(this);
                Thread.Sleep(5000);
            }
        }


        public void StopMonitoring()
        {
            this.monthread.Abort();
        }
    }
    public class MemcacheMonitor
    {
        int statsCapacity = 5;
        MemcacheClient mc;
        List<StatsBasic> basicStats;
        List<StatsMalloc> mallocStats;
        public MemcacheMonitor(MemcacheClient mc)
        {
            this.mc = mc;
            this.basicStats = new List<StatsBasic>();

        }

        public void GetStatsData()
        {
            this.basicStats.Add(this.mc.BasicStats());
            //this.mallocStats.Add(this.mc.MallocStats());

            this.discardOldStats();
        }
        private void discardOldStats()
        {
            this.discardOldStats(this.statsCapacity);
        }
        private void discardOldStats(int leaveCount)
        {
            //basic
            while (this.basicStats.Count > leaveCount)
            {
                this.basicStats.Remove(this.basicStats[0]);
            }
            //malloc
            //while (this.mallocStats.Count > leaveCount)
            //{
            //    this.mallocStats.Remove(this.mallocStats[0]);
            //}

        }

        public StatsDynamicBasic GetBasicStats()
        {
            return this.basicStats[this.basicStats.Count - 1].StatsDynamic(this.basicStats[this.basicStats.Count - 2]);
        }
        public String Name;


    }


}
