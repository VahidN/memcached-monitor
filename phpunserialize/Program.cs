﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.IO;
using thesebas.php;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] strings = new string[]{
                "N;",
                "b:1;",
                "b:0;",
                "i:122;",
                "i:-12234;",
                "d:10.5555555555555553581825733999721705913543701171875;",
                "d:-355.09090909090906507117324508726596832275390625;",
                "d:1.11E+7;",
                "s:12:\"abcedsmkljgl\";",
                "a:2:{i:0;s:12:\"abcedsmkljgl\";i:122;d:10.5555555555555553581825733999721705913543701171875;}",
                "O:8:\"stdClass\":2:{s:6:\"fieldA\";s:12:\"abcedsmkljgl\";s:6:\"fieldB\";a:2:{i:0;s:12:\"abcedsmkljgl\";i:122;d:10.5555555555555553581825733999721705913543701171875;}}",
                "O:2:\"XX\":4:{s:3:\"pub\";s:10:\"wartoscPub\";s:7:\"\0*\0prot\";s:11:\"wartoscProt\";s:8:\"\0XX\0priv\";s:15:\"wartoscPrywatna\";s:3:\"var\";s:10:\"wartoscVar\";}"
            };

            foreach (string s in strings) {
                PHPVariable v = thesebas.php.serialization.PHPUnserializer.Unserialize(s);
                Console.WriteLine(v);             
            }
            Console.ReadLine();
        }
    }
}
